#!/bin/bash 
VERSION=$1
for file in CA ESD US '' ISS invalid GBBC            
do
        #time java -cp ojdbc6.jar:.  RunMeGZ$file $VERSION > /dev/null
        NAME=${file}_
        if [ "${file}" == '' ]; then  NAME=''; fi
        if [ "${file}" == 'ESD' ]; then  NAME='sampling_'; fi
        tar cvf ebd_${NAME}rel${VERSION}.tar ebd_${NAME}rel${VERSION}.txt.gz -C /web/www/ebirddata.ornith.cornell.edu/downloads/metadata terms_of_use.txt recommended_citation.txt IBACodes.txt BCRCodes.txt USFWSCodes.txt eBird_Basic_Dataset_Metadata_v1.6.pdf
	echo cp ebd_${NAME}rel${VERSION}.tar /web/www/ebirddata.ornith.cornell.edu/downloads/ebd/prepackaged/
	
done

