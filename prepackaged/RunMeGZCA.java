import java.sql.*;
import java.io.*;
import java.util.zip.*;

public class RunMeGZCA {

  public static void main(String args[]) throws Exception {

    // WE DONT NEED NO STINKIN' TRY/CATCH/FINALLY

    Class.forName("oracle.jdbc.driver.OracleDriver");
    Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@tui.ornith.cornell.edu:1521:co2", "P_EBD", "sickbay_today");

    Statement stmt = conn.createStatement();
    stmt.setFetchSize(1000);
//    ResultSet rs = stmt.executeQuery("select * from ebird_basic_dataset"); // where species_code = 'amerob'");
    ResultSet rs = stmt.executeQuery("select global_unique_id as \"GLOBAL UNIQUE IDENTIFIER\", last_edited_dt as \"LAST EDITED DATE\", taxon_order as \"TAXONOMIC ORDER\",category,com_name as \"COMMON NAME\",sci_name as \"SCIENTIFIC NAME\",ssp_com_name as\"SUBSPECIES COMMON NAME\",ssp_sci_name as \"SUBSPECIES SCIENTIFIC NAME\",how_many as \"OBSERVATION COUNT\",bba_code as\"BREEDING BIRD ATLAS CODE\",age_sex as \"AGE/SEX\",country,country_code as \"COUNTRY CODE\",subnational1_name as state, subnational1_code as \"STATE CODE\", subnational2_name as county, subnational2_code as \"COUNTY CODE\",iba_code\"IBA CODE\",bcr_code\"BCR CODE\",usfws_code as \"USFWS CODE\", atlas_block as \"ATLAS BLOCK\",loc_name as locality,loc_id as \"LOCALITY ID\",loc_type as \" LOCALITY TYPE\",latitude,longitude,to_char(obs_dt,'YYYY-MM-DD') as \"OBSERVATION DATE\",obs_time as \"TIME OBSERVATIONS STARTED\",observer_id as \"OBSERVER ID\",first_name as \"FIRST NAME\",last_name as \"LAST NAME\",sub_id as \"SAMPLING EVENT IDENTIFIER\",protocol_desc as \"PROTOCOL TYPE\",proj_id as \"PROJECT CODE\",duration as \"DURATION MINUTES\",effort_distance_km as \"EFFORT DISTANCE KM\",effort_area_ha as \"EFFORT AREA HA\",num_observers as \"NUMBER OBSERVERS\",all_obs_reported as \"ALL SPECIES REPORTED\",group_id as \"GROUP IDENTIFIER\",has_media as \"HAS MEDIA\",valid approved,reviewed,reason,sub_comments as \"TRIP COMMENTS\",obs_comments as \"SPECIES COMMENTS\" from ebird_basic_dataset where country_code = 'CA'");

    int rsColumns = rs.getMetaData().getColumnCount();

//    OutputStreamWriter zip = new OutputStreamWriter( new GZIPOutputStream(new BufferedOutputStream(new FileOutputStream("P_EBD_gt.txt.gz"))), "UTF8" );
    GZIPOutputStream zip = new GZIPOutputStream(new BufferedOutputStream(new FileOutputStream("ebd_CA_rel"+args[0]+".txt.gz")));
//      zip.write("\ufeff".getBytes());

      for (int col=1; col<(rsColumns + 1); col++)  zip.write( ( rs.getMetaData().getColumnName(col) + "\t").getBytes());  // should probably use explicit charset
      zip.write("\n".getBytes());


    int rowCount = 0;
    while (rs.next()) {
      if (rowCount++ % 1000 == 0) System.out.println("Row: " + rowCount);
      for (int col=1; col<(rsColumns + 1); col++) { zip.write( ((rs.getObject(col) == null ? "" : rs.getString(col)) + "\t").getBytes()); 
      //  System.out.println(rs.getObject(col) == null ? "" : rs.getString(col)); 
      } // should probably use explicit charset
      zip.write("\n".getBytes());
    }

    zip.finish();
    zip.close();
  }
}
