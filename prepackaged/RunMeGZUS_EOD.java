import java.sql.*;
import java.io.*;
import java.util.zip.*;

public class RunMeGZUS_EOD {

  public static void main(String args[]) throws Exception {

    // WE DONT NEED NO STINKIN' TRY/CATCH/FINALLY

    Class.forName("oracle.jdbc.driver.OracleDriver");
    Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@tui.ornith.cornell.edu:1521:co2_tui", "P_EOD", "jazz2band3dancing");

    Statement stmt = conn.createStatement();
    stmt.setFetchSize(1000);
    ResultSet rs = stmt.executeQuery("SELECT BASISOFRECORD AS BASISOFRECORD, INSTITUTIONCODE AS INSTITUTIONCODE, COLLECTIONCODE AS COLLECTIONCODE, CATALOGNUMBER AS CATALOGNUMBER, OCCURRENCEID AS OCCURRENCEID, RECORDEDBY AS RECORDEDBY, YEAR AS YEAR, MONTH AS MONTH, DAY AS DAY, COUNTRY AS COUNTRY, STATEPROVINCE AS STATEPROVINCE, COUNTY AS COUNTY, DECIMALLATITUDE AS DECIMALLATITUDE, DECIMALLONGITUDE AS DECIMALLONGITUDE, LOCALITY AS LOCALITY, KINGDOM AS KINGDOM, PHYLUM AS PHYLUM, CLASS AS CLASS, ORDER_NAME AS \"ORDER\", FAMILY AS FAMILY, GENUS AS GENUS, SPECIFICEPITHET AS SPECIFICEPITHET, SCIENTIFICNAME AS SCIENTIFICNAME, VERNACULARNAME AS VERNACULARNAME, INDIVIDUALCOUNT AS INDIVIDUALCOUNT FROM p_eod.eod_obs_2013 WHERE country_code = 'US'");

   int rsColumns = rs.getMetaData().getColumnCount();

   GZIPOutputStream zip = new GZIPOutputStream(new BufferedOutputStream(new FileOutputStream("ebd_US_rel"+args[0]+".txt.gz")));

   for (int col=1; col<(rsColumns + 1); col++)  zip.write( ( rs.getMetaData().getColumnName(col) + "\t").getBytes());  // should probably use explicit charset
      zip.write("\n".getBytes());


    int rowCount = 0;
    while (rs.next()) {
      if (rowCount++ % 1000 == 0) System.out.println("Row: " + rowCount);
      for (int col=1; col<(rsColumns + 1); col++) { zip.write( ((rs.getObject(col) == null ? "" : rs.getString(col)) + "\t").getBytes()); 
      //  System.out.println(rs.getObject(col) == null ? "" : rs.getString(col)); 
      } // should probably use explicit charset
      zip.write("\n".getBytes());
    }

    zip.finish();
    zip.close();
  }
}
