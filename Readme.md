EBD Generation
==============

This repository contains the logic to generate the [EBD](https://confluence.cornell.edu/display/CLOIS/eBird+Basic+Dataset)

About Generation
----------------

EBD is composed by three target tables 

- EBIRD_BASIC_DATASET: Cntains all the valid, reviwed and provisional  data for all EBIRD* portals not incluing EBIRD_KIOSK and EBIRD_GBBC.
- EBIRD_BASIC_DATASET_GBBC: Separed tabe for EBIRD_GBBC project
- EBIRD_BASIC_DATASET_INVALID> Invalid records of EBIRD_KIOSK project

All tables are populated using the package EBD_GENERATION which execute the following tasks in the start_gen procedure:

1)	Refresh of the primary tables: Those tables are similar replicas of the eBird database. Materialized Views are used to bring the information to local tables. 

2)	Truncate and drop of indexes of target tables: using subroutines in the package UTL_CLO, the index ddl of each table are saved and dropped in oder to improve the performance of the refresh.

3)	Population of target tables: Tables are are populated using direct path on insert/select statements (append hint). 

    The view EBIRD_EBD_VIEW has the main query to feed all three tables. Data in the view is limited to observation date of the last day of the last month. Columns valid, review, proj_id and reason are used to provide the right set of data to the three target tables:

    - review: Boolean (1 or 0), indicate if the observation was reviewed. 
    - valid: Boolean (1 or 0), indicate if the observation is valid
    - proj_id: The project id of the checklist.
    - reason: Two values ( null or spexotic). The value spexotic indicates that observation was reviewed and approved with reason code spexotic. If null the observation can be in state valid or invalid as well as reviewed or not.

4)	Indexing of target tables: all three tables indexes are rebuild after data loading. 

Running and Monitoring Generation
---------------------------------

Before running the EBD generation remember to check:

- Table REF.TX_SPP_EBIRD contains the current version of the eBird Taxonomy.
- The metadata document for the new version.

To run the generation process just enable the job EBD_GENERATION under the schema P_EBD on the database CO2. 

```sql 
exec DBMS_SCHEDULER.enable(name=>'"P_EBD"."EBD_PRODUCTION"');
```

It is possible also schedule the job to run in night hours for better performance:
```sql 
BEGIN
DBMS_SCHEDULER.set_attribute( 
    name => '"P_EBD"."EBD_PRODUCTION"', 
    attribute => 'start_date', value => TO_TIMESTAMP_TZ('2017-04-24 20:00:00.000000000 AMERICA/NEW_YORK','YYYY-MM-DD HH24:MI:SS.FF TZR'));
DBMS_SCHEDULER.enable(name=>'"P_EBD"."EBD_PRODUCTION"');
END; 
/
```

To monitor the job use the 
```sql
SELECT JOB_NAME, STATE FROM ALL_SCHEDULER_JOBS WHERE JOB_NAME = 'EBD_PRODUCTION';
```

The table TASK_LOG contains detailed information about the task duration and records inserted.
```sql
select task_type, task_group, sub_task_name, status, duration, start_date from task_log order by task_id desc
```

Prepackaged Creation
--------------------

Once a new version of the EBD is on the target tables, is needed to build a set of files that contains data frequently demanded or which creation takes a long time ex: USA or CA canada dataset. 

You can run all set of prepackaged files using the bash script runAll.sh on server motmot01.ornith.cornell.edu. Change the first argument to reflect the current EBD version: ex: Feb-2017

```shell
cd /web/tmp/jdbcTest/
sudo nohup runAll.sh Feb-2017
# you can also use screen or tmux and run: sudo runAll.sh Feb-2017
```

this script will remove all current compresssed files 

TODO: move the files to the right server


