--------------------------------------------------------
--  DDL for Trigger TR_EBD_JOB_QUEUE_UPDATE
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER "TR_EBD_JOB_QUEUE_UPDATE" 
BEFORE UPDATE  ON P_EBD.EBD_JOB_QUEUE FOR EACH ROW
BEGIN
:NEW.edit_date := SYSDATE;
END;

/
ALTER TRIGGER "TR_EBD_JOB_QUEUE_UPDATE" ENABLE;
