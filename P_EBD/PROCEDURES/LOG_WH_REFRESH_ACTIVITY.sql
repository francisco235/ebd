--------------------------------------------------------
--  DDL for Procedure LOG_WH_REFRESH_ACTIVITY
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "LOG_WH_REFRESH_ACTIVITY" (  
   wh_name IN VARCHAR2,
   action IN VARCHAR2)
--Procedure to log events related to data warehouse refresh activity.
--Called explicity by refresh scripts to note interim step begin/completion.
-- Author:  Tom Fredericks
-- Date:  13 April 2011
IS  
PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
INSERT into p_ebd.WH_REFRESH_ACTIVITY_LOG(wh_name, action, action_dt)  values (wh_name, action, sysdate);
commit;  
END;
 

/
