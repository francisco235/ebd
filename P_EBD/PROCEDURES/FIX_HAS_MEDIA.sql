--------------------------------------------------------
--  DDL for Procedure FIX_HAS_MEDIA
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "FIX_HAS_MEDIA" is 
  task_id number;
  range_start date := to_date('01/01/1900','MM/DD/YYYY');
  range_end date:= to_date('01/01/1901','MM/DD/YYYY');
BEGIN
    LOOP    
      task_id := LOG_TASK.stared('FIX_MEDIA','2.0','Fixing data from '||range_start
                                  ||' to '||range_end);
      update /*+ INDEX(EBIRD_BASIC_DATASET EBD_OBS_ID_INDX) */ EBIRD_BASIC_DATASET
      set has_media=0
      where obs_dt>= range_start and obs_dt < range_end;
      
      LOG_TASK.successed(task_id,sql%rowcount);
      commit;
     
      range_start := range_end;
      range_end := range_start + interval '1' YEAR; 
      
      exit when range_start >= to_date('03/03/2018','MM/DD/YYYY');
      
    END LOOP;
       
  EXCEPTION WHEN OTHERS THEN 
    LOG_TASK.fail(task_id,SQLCODE,SQLERRM);
  END fix_has_media;

/
