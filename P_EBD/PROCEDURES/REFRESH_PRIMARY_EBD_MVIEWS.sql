--------------------------------------------------------
--  DDL for Procedure REFRESH_PRIMARY_EBD_MVIEWS
--------------------------------------------------------
set define off;

  CREATE OR REPLACE PROCEDURE "REFRESH_PRIMARY_EBD_MVIEWS" 
--Procedure to refresh the primary materialized views in the P_EBD schema on AT3.
-- Author:  Tom Fredericks
-- Date:  28 November 2011
-- Added OBS_AUX_MEDIA.  Corrected index INIT and NEXT parms.  tpl10 2017Mar6
-- Added OBS_OUT_STATUS_NDX, OBS_OUT_DT_NDX and SUB_OUT_PROJ_NDX fp235 2017 March 11
IS
BEGIN
execute immediate 'drop index P_EBD.LOC_NDX';
execute immediate 'drop index P_EBD.IX_MEDIA_OBS_ID';
execute immediate 'drop index P_EBD.LOC_AUX_IN_REGION_NDX';
execute immediate 'drop index  P_EBD.LOC_CLASS_NDX';
execute immediate 'drop index P_EBD.OBS_AUX_AS_NDX';
execute immediate 'drop index P_EBD.OBS_AUX_BC_NDX';
execute immediate 'drop index  P_EBD.OBS_OUT_NDX';
execute immediate 'drop index  P_EBD.OBS_OUT_STATUS_NDX';
execute immediate 'drop index  P_EBD.OBS_OUT_DT_NDX';
execute immediate 'drop index  P_EBD.OBS_REVIEW_NDX';
execute immediate 'drop index  P_EBD.PROTOCOL_NDX';
execute immediate 'drop index  P_EBD.SUB_OUT_NDX';
execute immediate 'drop index  P_EBD.SUB_OUT_PROJ_NDX';
execute immediate 'drop index P_EBD.USER1_NDX';
LOG_WH_REFRESH_ACTIVITY('EBD_PRIMARIES','Indexes Dropped');
execute immediate 'truncate table P_EBD.PROTOCOL';
execute immediate 'truncate table P_EBD.OBS_OUT';
execute immediate 'truncate table P_EBD.SUB_OUT';
execute immediate 'truncate table P_EBD.OBS_REVIEW';
execute immediate 'truncate table P_EBD.OBS_AUX_BC';
execute immediate 'truncate table P_EBD.OBS_AUX_AS';
execute immediate 'truncate table P_EBD.OBS_AUX_MEDIA';
execute immediate 'truncate table P_EBD.LOC_CLASS';
execute immediate 'truncate table P_EBD.LOC_AUX_IN_REGION';
execute immediate 'truncate table P_EBD.LOC';
execute immediate 'truncate table P_EBD.USER1';
LOG_WH_REFRESH_ACTIVITY('EBD_PRIMARIES','Tables Truncated');
DBMS_MVIEW.REFRESH('P_EBD.PROTOCOL,P_EBD.OBS_OUT,P_EBD.SUB_OUT,P_EBD.OBS_REVIEW,P_EBD.OBS_AUX_BC,P_EBD.OBS_AUX_AS,P_EBD.OBS_AUX_MEDIA,P_EBD.LOC,P_EBD.LOC_CLASS,P_EBD.LOC_AUX_IN_REGION,P_EBD.USER1','C','',FALSE,FALSE,0,0,0,TRUE);
LOG_WH_REFRESH_ACTIVITY('EBD_PRIMARIES','MViews Refreshed');
execute immediate 'CREATE INDEX P_EBD.LOC_NDX ON P_EBD.LOC
(LOC_ID, NAME, LATITUDE, LONGITUDE)
NOLOGGING
TABLESPACE EBIRD_BASIC_DATASET
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          100M
            NEXT             10M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL';
execute immediate 'CREATE INDEX P_EBD.IX_MEDIA_OBS_ID ON P_EBD.OBS_AUX_MEDIA
(OBS_ID)
NOLOGGING
TABLESPACE EBIRD_BASIC_DATASET
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          10M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
NOPARALLEL';
execute immediate 'CREATE INDEX "LOC_AUX_IN_REGION_NDX" ON "LOC_AUX_IN_REGION" 
("LOC_ID", "REGION_TYPE") 
NOLOGGING
TABLESPACE EBIRD_BASIC_DATASET
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          100M
            NEXT             10M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL';
execute immediate 'CREATE INDEX P_EBD.LOC_CLASS_NDX ON P_EBD.LOC_CLASS
(LOC_ID, CLASS)
NOLOGGING
TABLESPACE EBIRD_BASIC_DATASET
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          1M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL';
execute immediate 'CREATE INDEX "OBS_AUX_AS_NDX" ON "OBS_AUX_AS" 
("OBS_ID", "AGE_SEX") 
NOLOGGING
TABLESPACE EBIRD_BASIC_DATASET
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          100M
            NEXT             10M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL';
execute immediate 'CREATE INDEX P_EBD.OBS_AUX_BC_NDX ON P_EBD.OBS_AUX_BC
(OBS_ID, FIELD_NAME, AUX_CODE, NAME)
NOLOGGING
TABLESPACE EBIRD_BASIC_DATASET
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          100M
            NEXT             10M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL';
execute immediate 'CREATE INDEX P_EBD.OBS_OUT_NDX ON P_EBD.OBS_OUT
(SUB_ID, OBS_ID, SPECIES_CODE, ORIG_SPECIES_CODE, OBS_DT,
VALID, REVIEWED, HOW_MANY_ATLEAST, HOW_MANY_ATMOST, COMMENTS)
NOLOGGING
TABLESPACE EBIRD_BASIC_DATASET
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          100M
            NEXT             100M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL';
execute immediate 'CREATE INDEX P_EBD.OBS_OUT_STATUS_NDX ON P_EBD.OBS_OUT
(VALID, REVIEWED)
NOLOGGING
TABLESPACE EBIRD_BASIC_DATASET
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          100M
            NEXT             100M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL';
execute immediate 'CREATE INDEX P_EBD.OBS_OUT_DT_NDX ON P_EBD.OBS_OUT
(OBS_DT)
NOLOGGING
TABLESPACE EBIRD_BASIC_DATASET
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          100M
            NEXT             100M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL';
execute immediate 'CREATE INDEX P_EBD.OBS_REVIEW_NDX ON P_EBD.OBS_REVIEW
(OBS_ID, REASON_CODE)
NOLOGGING
TABLESPACE EBIRD_BASIC_DATASET
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          100M
            NEXT             100M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL';
execute immediate 'CREATE INDEX P_EBD.PROTOCOL_NDX ON P_EBD.PROTOCOL
(PROTOCOL_ID, DESCRIPTION)
NOLOGGING
TABLESPACE EBIRD_BASIC_DATASET
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL';
execute immediate 'CREATE INDEX P_EBD.SUB_OUT_NDX ON P_EBD.SUB_OUT
(SUB_ID, LOC_ID, PROTOCOL_ID, USER_ID)
NOLOGGING
TABLESPACE EBIRD_BASIC_DATASET
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          100M
            NEXT             10M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL';
execute immediate 'CREATE INDEX P_EBD.SUB_OUT_PROJ_NDX ON P_EBD.SUB_OUT
(PROJ_ID)
NOLOGGING
TABLESPACE EBIRD_BASIC_DATASET
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          100M
            NEXT             10M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL';
execute immediate 'CREATE INDEX P_EBD.USER1_NDX ON P_EBD.USER1
(USER_ID, LAST_NAME, FIRST_NAME)
NOLOGGING
TABLESPACE EBIRD_BASIC_DATASET
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          1M
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL';
LOG_WH_REFRESH_ACTIVITY('EBD_PRIMARIES','Indexes Rebuilt');
END;

/
