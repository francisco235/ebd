--------------------------------------------------------
--  DDL for Function LATEST_EBD_VERSION
--------------------------------------------------------

  CREATE OR REPLACE FUNCTION "LATEST_EBD_VERSION" RETURN VARCHAR2 IS

--***********************************
-- This function returns the latest version.
-- AUTHOR: Tim Levatich
-- DATE: 6 Dec 2016
--***********************************
version VARCHAR2(32);

BEGIN
SELECT version INTO version from ebd_version where LATEST_VERSION_FLAG = 1;
RETURN version;
END;

/
