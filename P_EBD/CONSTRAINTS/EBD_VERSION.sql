--------------------------------------------------------
--  Constraints for Table EBD_VERSION
--------------------------------------------------------

  ALTER TABLE "EBD_VERSION" MODIFY ("LATEST_VERSION_FLAG" NOT NULL ENABLE);
  ALTER TABLE "EBD_VERSION" MODIFY ("VERSION" NOT NULL ENABLE);
