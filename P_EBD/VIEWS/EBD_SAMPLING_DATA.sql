--------------------------------------------------------
--  DDL for View EBD_SAMPLING_DATA
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "EBD_SAMPLING_DATA" ("COUNTRY", "COUNTRY_CODE", "SUBNATIONAL1_NAME", "SUBNATIONAL1_CODE", "SUBNATIONAL2_NAME", "SUBNATIONAL2_CODE", "IBA_CODE", "BCR_CODE", "USFWS_CODE", "ATLAS_BLOCK", "LOC_NAME", "LOC_ID", "LOC_TYPE", "LATITUDE", "LONGITUDE", "OBS_DT", "OBS_TIME", "SUB_COMMENTS", "OBSERVER_ID", "FIRST_NAME", "LAST_NAME", "SUB_ID", "PROTOCOL_DESC", "PROJ_ID", "DURATION", "EFFORT_DISTANCE_KM", "EFFORT_AREA_HA", "NUM_OBSERVERS", "ALL_OBS_REPORTED", "GROUP_ID", "LAST_EDITED_DT") AS 
  SELECT tc.name AS country,
          tc.country_code,
          tx1.name AS subnational1_name,
          tx1.subnational1_code,
          tx2.name AS subnational2_name,
          tx2.subnational2_code,
          lair_iba.region_code AS iba_code,
          lair_bcr.region_code AS bcr_code,
          lair_usfws.region_code AS usfws_code,        
          (CASE
              WHEN (s.proj_id = 'EBIRD_ATL_WI')
              THEN
                 lair_gab.region_code
              ELSE
                 NULL
           END) atlas_block,
          REPLACE (REPLACE (REPLACE (l.NAME, CHR (10)), CHR (13)), CHR (9))
             AS loc_name,
          l.loc_id,
          DECODE (class,
                  'COUNTY', 'C',
                  'TOWN', 'T',
                  'SUBNAT1', 'S',
                  'POST_CODE', 'PC',
                  'BIRDING_HOTSPOT', 'H',
                  'P')
             loc_type,
          l.latitude,
          l.longitude,
          s.obs_dt,
          DECODE (s.obs_time_valid,
                  '1', TO_CHAR (s.obs_dt, 'HH24:MI:SS'),
                  NULL)
             obs_time,
          REPLACE (
             REPLACE (REPLACE (s.comments, CHR (10), '-'), CHR (13), '-'),
             CHR (9),
             ' ')
             sub_comments,
          REPLACE (u.user_id, 'USER', 'obs') AS observer_id,
          u.first_name AS first_name,
          u.last_name AS last_name,
          s.sub_id,
          p.description protocol_desc,
          s.proj_id,
          DECODE (s.duration_hrs, 0, NULL, ROUND (s.duration_hrs * 60))
             AS duration,
          s.effort_distance_km,
          s.effort_area_ha,
          s.num_observers,
          s.all_obs_reported,
          s.GROUP_ID,
          greatest(s.LAST_EDITED_DT, l.LAST_EDITED_DT) LAST_EDITED_DT
     FROM sub_out s
          JOIN protocol p ON s.protocol_id = p.protocol_id
          JOIN loc l ON s.loc_id = l.loc_id
          JOIN user1 u ON s.user_id = u.user_id
          JOIN REF.tx_country tc ON tc.country_code = l.country_code
          LEFT OUTER JOIN loc_aux_in_region lair_iba
             ON l.loc_id = lair_iba.loc_id AND lair_iba.region_type = 'IBA'
          LEFT OUTER JOIN loc_aux_in_region lair_bcr
             ON l.loc_id = lair_bcr.loc_id AND lair_bcr.region_type = 'BCR'
          LEFT OUTER JOIN loc_aux_in_region lair_gab on l.loc_id = lair_gab.loc_id AND lair_gab.region_type = 'GRID_ATLAS_BLOCK'
          LEFT OUTER JOIN loc_aux_in_region lair_usfws on l.loc_id = lair_usfws.loc_id AND lair_usfws.region_type = 'USFWS'
          LEFT OUTER JOIN loc_class lc ON l.loc_id = lc.loc_id
          LEFT OUTER JOIN REF.tx_subnational1 tx1
             ON l.subnational1_code = tx1.subnational1_code
          LEFT OUTER JOIN REF.tx_subnational2 tx2
             ON l.subnational2_code = tx2.subnational2_code;
