--------------------------------------------------------
--  DDL for View EBIRD_EBD_VIEW
--------------------------------------------------------

  CREATE OR REPLACE FORCE VIEW "EBIRD_EBD_VIEW" ("GLOBAL_UNIQUE_ID", "TAXON_ORDER", "CATEGORY", "SPECIES_CODE", "ORIG_SPECIES_CODE", "COM_NAME", "SCI_NAME", "SSP_COM_NAME", "SSP_SCI_NAME", "HOW_MANY", "BBA_CODE", "AGE_SEX", "OBS_ID", "COUNTRY", "COUNTRY_CODE", "SUBNATIONAL1_NAME", "SUBNATIONAL1_CODE", "SUBNATIONAL2_NAME", "SUBNATIONAL2_CODE", "REGION_CODE", "IBA_CODE", "BCR_CODE", "USFWS_CODE", "ATLAS_CODE", "LOC_NAME", "LOC_ID", "LOC_TYPE", "LATITUDE", "LONGITUDE", "OBS_DT", "OBS_TIME", "YYYY", "MM", "DD", "SUB_COMMENTS", "OBS_COMMENTS", "OBSERVER_ID", "FIRST_NAME", "LAST_NAME", "SUB_ID", "PROJ_ID", "PROTOCOL_DESC", "DURATION", "EFFORT_DISTANCE_KM", "EFFORT_AREA_HA", "NUM_OBSERVERS", "ALL_OBS_REPORTED", "GROUP_ID", "VALID", "REVIEWED", "REASON", "HAS_MEDIA", "LAST_EDITED_DT") AS 
  SELECT 'URN:CornellLabOfOrnithology:EBIRD:'||o.obs_id global_unique_id,
       e2.taxon_order taxon_order,
       e2.category AS CATEGORY,
       o.species_code, 
       o.orig_species_code,
       e.primary_com_name com_name,
       e.sci_name,
       DECODE (orig_species_code, e.species_code, NULL, e2.primary_com_name) ssp_com_name,
       DECODE (orig_species_code, e.species_code, NULL, e2.sci_name) ssp_sci_name,
       DECODE (o.how_many_atleast,
               o.how_many_atmost, TO_CHAR (o.how_many_atleast),
               'X') how_many,
       boa.aux_code bba_code,
       oas.age_sex,
       o.obs_id,
       tc.name country,
       tc.country_code,
       tx1.name subnational1_name,
       tx1.subnational1_code ,
       tx2.name subnational2_name,
       tx2.subnational2_code,
       null  "REGION_CODE",
       lair_iba.region_code iba_code,
       lair_bcr.region_code bcr_code,
       lair_usfws.region_code usfws_code,
       (CASE
              WHEN (s.proj_id = 'EBIRD_ATL_WI')
              THEN
                 lair_gab.region_code
              ELSE
                 NULL
           END) atlas_code,
       REPLACE (REPLACE (REPLACE (l.NAME, CHR (10)), CHR (13)), CHR (9)) loc_name,
       l.loc_id,
       decode(class,'BIRDING_HOTSPOT','H','COUNTY','C','POST_CODE','PC','SUBNAT1','S','TOWN','T','P') loc_type,
       l.latitude,
       l.longitude,
       o.obs_dt,
       DECODE (s.obs_time_valid, '1', TO_CHAR (o.obs_dt, 'HH24:MI:SS'), NULL) obs_time,
       TO_CHAR (o.obs_dt, 'YYYY') yyyy,
       TO_CHAR (o.obs_dt, 'MM') mm,
       TO_CHAR (o.obs_dt, 'DD') dd, 
       REPLACE (REPLACE (REPLACE (s.comments, CHR (10), '-'), CHR (13), '-'),
                CHR (9),
                ' ') sub_comments,
       REPLACE (REPLACE (REPLACE (o.comments, CHR (10), '-'), CHR (13), '-'),
                CHR (9),
                ' ') obs_comments,
       REPLACE (u.user_id, 'USER', 'obsr') observer_id,
       u.first_name,
       u.last_name,
       s.sub_id,
       s.proj_id,
       p.description protocol_desc,
       DECODE (s.duration_hrs, 0, NULL, ROUND (s.duration_hrs * 60)) duration,
       s.effort_distance_km,
       s.effort_area_ha,
       s.num_observers,
       s.all_obs_reported,
       s.GROUP_ID,
       O.VALID,
       O.REVIEWED,
       DECODE (orr.reason_code, 'spexotic', 'Introduced/Exotic', NULL) reason,
       (case when media.obs_id is null then '0' else '1' end) has_media,
       greatest(l.last_edited_dt, s.last_edited_dt, o.last_edited_dt) last_edited_dt
  FROM p_ebd.sub_out s
       JOIN p_ebd.protocol p
          ON s.protocol_id = p.protocol_id
       JOIN p_ebd.loc l
          ON s.loc_id = l.loc_id
       JOIN p_ebd.obs_out o
          ON s.sub_id = o.sub_id
       JOIN p_ebd.user1 u
          ON s.user_id = u.user_id
       JOIN tx_spp_ebird e
          ON o.species_code = e.species_code
       JOIN tx_spp_ebird e2
          ON (o.orig_species_code = e2.species_code)
       LEFT OUTER JOIN p_ebd.obs_aux_bc boa
          ON o.obs_id = boa.obs_id AND boa.field_name = 'breeding_code'
       LEFT OUTER JOIN p_ebd.obs_aux_as oas
          ON o.obs_id = oas.obs_id
       LEFT OUTER JOIN p_ebd.obs_review orr
          ON o.obs_id = orr.obs_id AND orr.reason_code = 'spexotic'
       JOIN ref.tx_country tc
          ON tc.country_code = l.country_code
       LEFT OUTER JOIN p_ebd.loc_aux_in_region lair_iba on l.loc_id = lair_iba.loc_id AND lair_iba.region_type = 'IBA'
       LEFT OUTER JOIN p_ebd.loc_aux_in_region lair_bcr on l.loc_id = lair_bcr.loc_id AND lair_bcr.region_type = 'BCR'
       LEFT OUTER JOIN p_ebd.loc_aux_in_region lair_gab on l.loc_id = lair_gab.loc_id AND lair_gab.region_type = 'GRID_ATLAS_BLOCK'
       LEFT OUTER JOIN p_ebd.loc_aux_in_region lair_usfws on l.loc_id = lair_usfws.loc_id AND lair_usfws.region_type = 'USFWS'
       LEFT OUTER JOIN p_ebd.obs_aux_media media on o.obs_id=media.obs_id
       LEFT OUTER JOIN ref.tx_subnational1 tx1
          ON l.subnational1_code = tx1.subnational1_code
       LEFT OUTER JOIN ref.tx_subnational2 tx2
          ON l.subnational2_code = tx2.subnational2_code
       LEFT OUTER JOIN p_ebd.loc_class lc on lc.loc_id = s.loc_id
WHERE s.obs_dt < TO_DATE ('01/'||TO_CHAR(SYSDATE, 'MM/YYYY'), 'DD/MM/YYYY') 
  and o.obs_dt < TO_DATE ('01/'||TO_CHAR(SYSDATE, 'MM/YYYY'), 'DD/MM/YYYY');
