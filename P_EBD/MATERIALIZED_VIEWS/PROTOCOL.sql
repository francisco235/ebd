--------------------------------------------------------
--  DDL for Materialized View PROTOCOL
--------------------------------------------------------

  CREATE MATERIALIZED VIEW "PROTOCOL" ("PROTOCOL_ID", "DESCRIPTION")
  ON PREBUILT TABLE WITH REDUCED PRECISION
  USING INDEX 
  REFRESH COMPLETE ON DEMAND
  USING DEFAULT LOCAL ROLLBACK SEGMENT
  USING ENFORCED CONSTRAINTS DISABLE QUERY REWRITE
  AS SELECT distinct protocol_id, description
  FROM    cur.protocol@sw2.ornith.cornell.edu
       JOIN
          cur.proj_protocol@sw2.ornith.cornell.edu
       USING (protocol_id)
 WHERE proj_id LIKE 'EBIRD%';

   COMMENT ON MATERIALIZED VIEW "PROTOCOL"  IS 'Copy of production PROTOCOL used to build EBD';
