--------------------------------------------------------
--  DDL for Materialized View OBS_REVIEW
--------------------------------------------------------

  CREATE MATERIALIZED VIEW "OBS_REVIEW" ("OBS_ID", "REASON_CODE")
  ON PREBUILT TABLE WITH REDUCED PRECISION
  USING INDEX 
  REFRESH COMPLETE ON DEMAND
  USING DEFAULT LOCAL ROLLBACK SEGMENT
  USING ENFORCED CONSTRAINTS DISABLE QUERY REWRITE
  AS SELECT obs_id, reason_code
  FROM obs_review@sw2.ornith.cornell.edu
 WHERE (obs_id, action_dt ) IN
 (  SELECT obs_id, max(action_dt) max_action_dt
      FROM obs_review@sw2.ornith.cornell.edu
     WHERE reviewer_user_id <> 'USER0' AND proj_id LIKE 'EBIRD%' AND station_id = 'S0'
  GROUP BY obs_id);

   COMMENT ON MATERIALIZED VIEW "OBS_REVIEW"  IS 'Copy of production OBS_REVIEW used to build EBD';
