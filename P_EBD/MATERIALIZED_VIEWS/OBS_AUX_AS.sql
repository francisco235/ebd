--------------------------------------------------------
--  DDL for Materialized View OBS_AUX_AS
--------------------------------------------------------

  CREATE MATERIALIZED VIEW "OBS_AUX_AS" ("OBS_ID", "FIELD_NAME", "AGE_SEX")
  ON PREBUILT TABLE WITH REDUCED PRECISION
  USING INDEX 
  REFRESH COMPLETE ON DEMAND
  USING DEFAULT LOCAL ROLLBACK SEGMENT
  USING ENFORCED CONSTRAINTS DISABLE QUERY REWRITE
  AS SELECT obs_id, field_name,
   LISTAGG( get_text@sw2.ornith.cornell.edu(name,'en','en')||' ('||VALUE||')'
              , '; ') WITHIN GROUP (ORDER BY field_name)  AS age_sex
  FROM obs_aux@sw2.ornith.cornell.edu
  JOIN tx_aux_field_code@sw2.ornith.cornell.edu USING (field_name, aux_code)
  WHERE field_name='age_sex'
  GROUP BY obs_id, field_name;

   COMMENT ON MATERIALIZED VIEW "OBS_AUX_AS"  IS 'Copy of production OBS_AUX used to build EBD, restricted to Age/Sex';
