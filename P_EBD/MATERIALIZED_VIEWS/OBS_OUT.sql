--------------------------------------------------------
--  DDL for Materialized View OBS_OUT
--------------------------------------------------------

  CREATE MATERIALIZED VIEW "OBS_OUT" ("OBS_ID", "SUB_ID", "SPECIES_CODE", "ORIG_SPECIES_CODE", "OBS_DT", "YYYYMM", "HOW_MANY_ATLEAST", "HOW_MANY_ATMOST", "COMMENTS", "VALID", "REVIEWED", "LAST_EDITED_DT")
  ON PREBUILT TABLE WITH REDUCED PRECISION
  USING INDEX 
  REFRESH COMPLETE ON DEMAND
  USING DEFAULT LOCAL ROLLBACK SEGMENT
  USING ENFORCED CONSTRAINTS DISABLE QUERY REWRITE
  AS SELECT obs_id,
       sub_id,
       species_code,
       orig_species_code,
       obs_dt,
       TO_CHAR (obs_dt, 'YYYYMM') YYYYMM,
       how_many_atleast,
       how_many_atmost,
       (CASE
            WHEN INSTR (comments, '"') > 0 AND INSTR (comments, '"', 2) < 1
            THEN
                SUBSTR (comments, 2)
            ELSE
                comments
        END),
       valid,
       reviewed,
       last_edited_dt
  FROM obs_out@sw2.ornith.cornell.edu;

   COMMENT ON MATERIALIZED VIEW "OBS_OUT"  IS 'Copy of production OBS_OUT used to build EBD';
