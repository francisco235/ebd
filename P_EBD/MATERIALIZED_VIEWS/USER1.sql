--------------------------------------------------------
--  DDL for Materialized View USER1
--------------------------------------------------------

  CREATE MATERIALIZED VIEW "USER1" ("USER_ID", "FIRST_NAME", "LAST_NAME")
  ON PREBUILT TABLE WITH REDUCED PRECISION
  USING INDEX 
  REFRESH COMPLETE ON DEMAND
  USING DEFAULT LOCAL ROLLBACK SEGMENT
  USING ENFORCED CONSTRAINTS DISABLE QUERY REWRITE
  AS SELECT user_id, replace(first_name, chr(9), ''), replace(last_name, chr(9), '')
  FROM user1@sw2.ornith.cornell.edu;

   COMMENT ON MATERIALIZED VIEW "USER1"  IS 'Copy of production USER1 used to build EBD';
