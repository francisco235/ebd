--------------------------------------------------------
--  DDL for Materialized View SUB_OUT
--------------------------------------------------------

  CREATE MATERIALIZED VIEW "SUB_OUT" ("SUB_ID", "LOC_ID", "PROJ_ID", "LAST_EDITED_DT", "OBS_DT", "OBS_TIME_VALID", "ALL_OBS_REPORTED", "USER_ID", "NUM_OBSERVERS", "PROTOCOL_ID", "DURATION_HRS", "EFFORT_DISTANCE_KM", "EFFORT_AREA_HA", "COMMENTS", "GROUP_ID")
  ON PREBUILT TABLE WITH REDUCED PRECISION
  USING INDEX 
  REFRESH COMPLETE ON DEMAND
  USING DEFAULT LOCAL ROLLBACK SEGMENT
  USING ENFORCED CONSTRAINTS DISABLE QUERY REWRITE
  AS SELECT sub_id,
       loc_id,
       proj_id,
       last_edited_dt,
       obs_dt,
       obs_time_valid,
       all_obs_reported,
       user_id,
       num_observers,
       protocol_id,
       duration_hrs,
       effort_distance_km,
       effort_area_ha,
       (CASE
            WHEN INSTR (comments, '"') > 0 AND INSTR (comments, '"', 2) < 1
            THEN
                SUBSTR (comments, 2)
            ELSE
                comments
        END),
       GROUP_ID
  FROM sub_OUT@sw2.ornith.cornell.edu;

   COMMENT ON MATERIALIZED VIEW "SUB_OUT"  IS 'Copy of production SUB_OUT used to build EBD';
