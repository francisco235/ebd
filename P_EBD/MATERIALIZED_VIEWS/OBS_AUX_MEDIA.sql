--------------------------------------------------------
--  DDL for Materialized View OBS_AUX_MEDIA
--------------------------------------------------------

  CREATE MATERIALIZED VIEW "OBS_AUX_MEDIA" ("OBS_ID")
  ON PREBUILT TABLE WITH REDUCED PRECISION
  USING INDEX 
  REFRESH COMPLETE ON DEMAND
  USING DEFAULT LOCAL ROLLBACK SEGMENT
  USING ENFORCED CONSTRAINTS DISABLE QUERY REWRITE
  AS SELECT distinct obs_id
  FROM CUR.obs_aux_media@sw2.ornith.cornell.edu;

   COMMENT ON MATERIALIZED VIEW "OBS_AUX_MEDIA"  IS 'Copy of production OBS_AUX_MEDIA OBS_IDs used to build EBD';
