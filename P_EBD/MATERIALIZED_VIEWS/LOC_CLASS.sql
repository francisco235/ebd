--------------------------------------------------------
--  DDL for Materialized View LOC_CLASS
--------------------------------------------------------

  CREATE MATERIALIZED VIEW "LOC_CLASS" ("CLASS", "LOC_ID")
  ON PREBUILT TABLE WITH REDUCED PRECISION
  USING INDEX 
  REFRESH COMPLETE ON DEMAND
  USING DEFAULT LOCAL ROLLBACK SEGMENT
  USING ENFORCED CONSTRAINTS DISABLE QUERY REWRITE
  AS SELECT DECODE (class,
               'US_POST_CODE', 'POST_CODE',
               'CAN_POST_CODE', 'POST_CODE',
               'PR_POST_CODE', 'POST_CODE',
               'VI_POST_CODE', 'POST_CODE',
               class)
          AS class,
       loc_id
  FROM loc_class@sw2.ornith.cornell.edu
 WHERE  class IN
              ('US_POST_CODE',
               'CAN_POST_CODE',
               'TOWN',
               'COUNTY',
               'SUBNAT1',
               'BIRDING_HOTSPOT',
               'PR_POST_CODE',
               'VI_POST_CODE');

   COMMENT ON MATERIALIZED VIEW "LOC_CLASS"  IS 'Copy of production LOC_CLASS used to build EBD';
