--------------------------------------------------------
--  DDL for Materialized View OBS_AUX_BC
--------------------------------------------------------

  CREATE MATERIALIZED VIEW "OBS_AUX_BC" ("OBS_ID", "FIELD_NAME", "NAME", "AUX_CODE")
  ON PREBUILT TABLE WITH REDUCED PRECISION
  USING INDEX 
  REFRESH COMPLETE ON DEMAND
  USING DEFAULT LOCAL ROLLBACK SEGMENT
  USING ENFORCED CONSTRAINTS DISABLE QUERY REWRITE
  AS SELECT obs_id,
       field_name,
       get_text@sw2.ornith.cornell.edu (name, 'en', 'es') AS name,
       get_text@sw2.ornith.cornell.edu (name, 'en', 'es') AS aux_code
  FROM obs_aux@sw2.ornith.cornell.edu
       JOIN tx_aux_field_code@sw2.ornith.cornell.edu
           USING (field_name, aux_code)
 WHERE     field_name = 'breeding_code';

   COMMENT ON MATERIALIZED VIEW "OBS_AUX_BC"  IS 'Copy of production OBS_AUX used to build EBD, restricted to Breeding Codes';
