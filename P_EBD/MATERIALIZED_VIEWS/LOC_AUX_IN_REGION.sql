--------------------------------------------------------
--  DDL for Materialized View LOC_AUX_IN_REGION
--------------------------------------------------------

  CREATE MATERIALIZED VIEW "LOC_AUX_IN_REGION" ("LOC_ID", "REGION_TYPE", "REGION_CODE")
  ON PREBUILT TABLE WITH REDUCED PRECISION
  USING INDEX 
  REFRESH COMPLETE ON DEMAND
  WITH PRIMARY KEY USING DEFAULT LOCAL ROLLBACK SEGMENT
  USING ENFORCED CONSTRAINTS DISABLE QUERY REWRITE
  AS SELECT loc_id, region_type, region_code
  FROM loc_aux_in_region@sw2.ornith.cornell.edu
 WHERE region_type IN ('IBA', 'BCR', 'GRID_ATLAS_BLOCK', 'USFWS');

   COMMENT ON MATERIALIZED VIEW "LOC_AUX_IN_REGION"  IS 'Copy of production LOC_AUX_IN_REGION used to build EBD';
