--------------------------------------------------------
--  DDL for Package Body UTL_CLO
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "UTL_CLO" AS
   
   /*
    Refresh views and log the operation time 
   */
   PROCEDURE execute_refresh_me(me_views varchar2) is 
   task_id number;
   BEGIN
     task_id := LOG_TASK.stared('RMV','Execution','Refresh: '||me_views);
     DBMS_MVIEW.REFRESH(me_views,'C','',FALSE,FALSE,0,0,0,TRUE);
     LOG_TASK.successed(task_id, null);
   EXCEPTION WHEN OTHERS THEN 
      LOG_TASK.fail(task_id,SQLCODE,SQLERRM);
      IF INSTR(SQLERRM,'ORA-01555') > 0 THEN
        BEGIN
          task_id := LOG_TASK.stared('RMV','Execution','Refresh(2nd time): '||me_views); 
          DBMS_MVIEW.REFRESH(me_views,'C','',FALSE,FALSE,0,0,0,TRUE);
          LOG_TASK.successed(task_id, null);
        EXCEPTION WHEN OTHERS THEN 
          LOG_TASK.fail(task_id,SQLCODE,SQLERRM);
          raise;
        END;
      ELSE
        raise;
      END IF;        
   END execute_refresh_me;
   
   PROCEDURE refresh_materialized_views(materialized_views in StringArray) IS
   idx number;
   task_id number;
   me_view varchar2(255);
   me_views_str varchar2(4000):='';
   schema varchar2(255);
   mv_name varchar2(255);
   BEGIN
      -- loop for disable indexes and truncate tables
      idx := materialized_views.FIRST;
      me_view := materialized_views(idx);
      WHILE idx IS NOT NULL LOOP
         me_view := materialized_views(idx);
         disable_indexes(me_view);
         truncate_table(me_view);
         me_views_str := me_views_str||me_view;
         idx := materialized_views.NEXT(idx); 
         if idx IS NOT NULL then
            me_views_str := me_views_str||',';
         end if;
         -- refresh individual materializaed view
         -- execute_refresh_me(me_view);
      END LOOP;
      -- refresh all materialized views 
      execute_refresh_me(me_views_str);
      
      -- enable the indexes and calculate stats
      task_id := LOG_TASK.stared('RMV','UTL_CLO','Index Rebuilding and Stats Calculation');      
      idx := materialized_views.FIRST;
      me_view := materialized_views(idx);
      WHILE idx IS NOT NULL LOOP
         me_view := materialized_views(idx);
         if regexp_instr(me_view,'(.*)\.(.*)')>0 then 
            schema  := regexp_replace(me_view,'(.*)\.(.*)','\1'); 
            mv_name := regexp_replace(me_view,'(.*)\.(.*)','\2'); 
            DBMS_STATS.GATHER_TABLE_STATS(schema,mv_name);
         else 
            DBMS_STATS.GATHER_TABLE_STATS(USER,me_view);
         end if;
        restore_indexes(me_view);
         
        idx := materialized_views.NEXT(idx); 
      END LOOP;
      LOG_TASK.successed(task_id,0);
      commit;
    EXCEPTION WHEN OTHERS THEN 
      LOG_TASK.fail(task_id,SQLCODE,SQLERRM);
      raise;
   END refresh_materialized_views;
   
   -- ===========================================================================  
  
  procedure save_ddl(the_object_name varchar2, the_object_type varchar2, a_group_name varchar2 default 'default_group') is
    object_ddl varchar2(4000);
  begin 
    object_ddl := dbms_metadata.get_ddl(the_object_type,the_object_name);
    MERGE INTO OBJECT_DDLS d USING (select 1 from dual) 
    ON (d.object_name = the_object_name and d.group_name=a_group_name)
    WHEN MATCHED THEN UPDATE SET d.ddl_text=object_ddl, d.generated=current_timestamp
    WHEN NOT MATCHED THEN INSERT (object_name, ddl_text, generated, group_name) 
          VALUES (the_object_name, object_ddl, current_timestamp, a_group_name );
    commit;
  end save_ddl;
  
  procedure execute_ddl(the_object_name varchar2, group_name varchar2 default 'default_group') is
    ddl_text varchar2(4000);
  begin
    select ddl_text into ddl_text from object_ddls where 
        object_name=the_object_name and group_name=group_name;
    if (ddl_text is not null) then
      ddl_text := trim(trailing ';' from ddl_text );
      -- DBMS_OUTPUT.PUT_LINE('Executing DDL: '||ddl_text);
      execute immediate ddl_text;
      update object_ddls set executed=current_timestamp where object_name=the_object_name;
    end if;
  end execute_ddl;
  
  procedure execute_group_ddl(a_group_name varchar2 default 'default_group') is
    ddl_text varchar2(4000);
  begin
    for obj_ddl in (select * from object_ddls where group_name=a_group_name) loop
      if (obj_ddl.ddl_text is not null) then
        ddl_text := trim(trailing ';' from obj_ddl.ddl_text );
        DBMS_OUTPUT.PUT_LINE('Executing DDL: '||ddl_text);
        execute immediate ddl_text;
        update object_ddls set executed=current_timestamp 
            where object_name=obj_ddl.object_name and group_name=a_group_name;
      end if;
    end loop;
  end execute_group_ddl;
  
  procedure delete_ddl_group(a_group_name varchar2 default 'default_group') is
  begin
    delete from object_ddls where group_name=a_group_name;
  end delete_ddl_group;
  
  
  /*
   Get the ddl of the indexes belowing to tables in indexed tables, 
   write the ddl in the file
   */
   PROCEDURE disable_indexes(indexed_table varchar2) IS
      ddl_statement varchar2(1024);
      out_File utl_file.file_type;
      idx number;
      task_id number;
   BEGIN
       DBMS_OUTPUT.PUT_LINE('Disabling indexes of table '||indexed_table);
       delete_ddl_group(indexed_table); 
       FOR ind in (select index_name from USER_INDEXES where table_name=upper(indexed_table)
                    or TABLE_OWNER||'.'||TABLE_NAME = upper(indexed_table)) LOOP
          DBMS_OUTPUT.PUT_LINE(' - Index: '||ind.index_name||' of table '||indexed_table);
          save_ddl(ind.index_name,'INDEX', indexed_table);
          drop_index_if_exists(ind.index_name);
       END LOOP;         
  END disable_indexes;
  
  /*
    Restore the indexes saved in disabled routine, remove the created file
  */  
  PROCEDURE restore_indexes(indexed_table varchar2) IS
  BEGIN
     execute_group_ddl(indexed_table);
  END restore_indexes;
  
  PROCEDURE drop_index_if_exists(index_name varchar2) is 
  BEGIN
    EXECUTE IMMEDIATE 'DROP INDEX ' || index_name;
  EXCEPTION
    WHEN OTHERS THEN
      IF SQLCODE != -1418 THEN
        RAISE;
      END IF;
  END;

  PROCEDURE set_index_unusable(index_name varchar2) IS
  BEGIN
      EXECUTE IMMEDIATE 'ALTER INDEX ' || index_name || ' UNUSABLE';
  END;
 
  PROCEDURE rebuild_index(index_name varchar2) IS
  BEGIN
      EXECUTE IMMEDIATE 'ALTER INDEX ' || index_name || ' REBUILD';
  END;
  
  PROCEDURE set_table_indexes_unusable(a_table_name varchar2) IS
  BEGIN
     FOR ind in (select index_name from USER_INDEXES where table_name=upper(a_table_name)
                    or TABLE_OWNER||'.'||TABLE_NAME = upper(a_table_name) ) LOOP
       DBMS_OUTPUT.PUT_LINE(' - Index: '||ind.index_name||' of table '||a_table_name); 
       set_index_unusable(ind.index_name);
     END LOOP;
  END set_table_indexes_unusable;
  
  PROCEDURE rebuild_table_indexes(a_table_name varchar2) IS
  BEGIN
      FOR ind in (select index_name from USER_INDEXES where table_name=upper(a_table_name)
                    or TABLE_OWNER||'.'||TABLE_NAME = upper(a_table_name) ) LOOP
          rebuild_index(ind.index_name);
       END LOOP;
  END rebuild_table_indexes;
   
  PROCEDURE truncate_table(table_name varchar2, reuse_storage boolean default true) IS
  BEGIN
      EXECUTE IMMEDIATE 'TRUNCATE TABLE ' || table_name ;
  END; 
  
  PROCEDURE prepare_table_load(table_name varchar2) IS
  BEGIN
      truncate_table(table_name);
      set_table_indexes_unusable(table_name);
  END prepare_table_load;
  
END UTL_CLO;

/
