--------------------------------------------------------
--  DDL for Package Body EBD_GENERATION
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "EBD_GENERATION" AS
    
  indexed_tables StringList := StringList('EBIRD_BASIC_DATASET', 'EBIRD_BASIC_DATASET_GBBC','EBIRD_BASIC_DATASET_INVALID');
  materialized_views utl_clo.StringArray := utl_clo.StringArray('P_EBD.LOC','P_EBD.LOC_CLASS',
    'P_EBD.LOC_AUX_IN_REGION','P_EBD.OBS_AUX_AS','P_EBD.OBS_AUX_BC','P_EBD.OBS_AUX_MEDIA','P_EBD.OBS_OUT',
    'P_EBD.OBS_REVIEW','P_EBD.PROTOCOL','P_EBD.SUB_OUT','P_EBD.USER1');
  
  procedure save_ddl(the_object_name varchar2, the_object_type varchar2) is
    object_ddl varchar2(4000);
  begin 
    object_ddl := dbms_metadata.get_ddl(the_object_type,the_object_name);
    MERGE INTO OBJECT_DDLS d USING (select 1 from dual) 
    ON (d.object_name = the_object_name)
    WHEN MATCHED THEN UPDATE SET d.ddl_text=object_ddl, d.generated=current_timestamp
    WHEN NOT MATCHED THEN INSERT (object_name, ddl_text, generated) VALUES (the_object_name, object_ddl, current_timestamp );
    commit;
  end save_ddl;
  
  procedure execute_ddl(the_object_name varchar2) is
    ddl_text varchar2(4000);
  begin
    select ddl_text into ddl_text from object_ddls where object_name=the_object_name;
    if (ddl_text is not null) then
      ddl_text := trim(trailing ';' from ddl_text );
      DBMS_OUTPUT.PUT_LINE('Executing DDL: '||ddl_text);
      execute immediate ddl_text;
      update object_ddls set executed=current_timestamp where object_name=the_object_name;
    end if;
  end execute_ddl;
  
  /*
    Refresh views and log the operation time 
   */
   PROCEDURE refresh_views(version_ varchar2) IS
   task_id number;
   stack_trace varchar2(4000);
   BEGIN
      task_id := LOG_TASK.stared('EBD',version_,'REFRESH EBD VIEW');
      UTL_CLO.REFRESH_MATERIALIZED_VIEWS(materialized_views);
      LOG_TASK.successed(task_id,sql%rowcount);
      commit;
    EXCEPTION WHEN OTHERS THEN 
      LOG_TASK.fail(task_id,SQLCODE,SQLERRM);
      raise;
   END refresh_views;
   
   /*
   Get the ddl of the indexes belowing to tables in indexed tables, write the ddl in the file and drop the indexes
   */
   PROCEDURE disable_indexes(version_ varchar2) IS
      indexed_table varchar(100);
      ddl_statement varchar2(1024);
      out_File utl_file.file_type;
      idx number;
      task_id number;
   BEGIN
      task_id := LOG_TASK.stared('EBD',version_,'Disable Indexes');
      idx := indexed_tables.FIRST;
      WHILE idx IS NOT NULL LOOP
         indexed_table := indexed_tables(idx);
         DBMS_OUTPUT.PUT_LINE('Processing Table '||indexed_table);

         FOR ind in (select index_name from USER_INDEXES where table_name=indexed_table) LOOP
            DBMS_OUTPUT.PUT_LINE(' - Index: '||ind.index_name||' of table '||indexed_table);
            save_ddl(ind.index_name,'INDEX');
            EXECUTE IMMEDIATE 'drop index '||ind.index_name;
            -- EXECUTE IMMEDIATE 'alter index '||ind.index_name||' unusable';
         END LOOP;         
         EXECUTE IMMEDIATE 'truncate table '||indexed_table;
        idx := indexed_tables.NEXT(idx); 
      END LOOP;
      LOG_TASK.successed(task_id, null);
    EXCEPTION WHEN OTHERS THEN 
      LOG_TASK.fail(task_id,SQLCODE,SQLERRM);
  END disable_indexes;
  
  PROCEDURE disable_indexes2(version_ varchar2) IS
      indexed_table varchar(100);
      ddl_statement varchar2(1024);
      out_File utl_file.file_type;
      idx number;
      task_id number;
   BEGIN
      task_id := LOG_TASK.stared('EBD',version_,'Disable Indexes');
      idx := indexed_tables.FIRST;
      WHILE idx IS NOT NULL LOOP
         indexed_table := indexed_tables(idx);
         DBMS_OUTPUT.PUT_LINE('Processing Table '||indexed_table);

         FOR ind in (select index_name from USER_INDEXES where table_name=indexed_table) LOOP
            DBMS_OUTPUT.PUT_LINE(' - Index: '||ind.index_name||' of table '||indexed_table);
            save_ddl(ind.index_name,'INDEX');
            EXECUTE IMMEDIATE 'alter index '||ind.index_name||' usable';
         END LOOP;         
         EXECUTE IMMEDIATE 'tuncate table '||indexed_table;
        idx := indexed_tables.NEXT(idx); 
      END LOOP;
      LOG_TASK.successed(task_id, null);
    EXCEPTION WHEN OTHERS THEN 
      LOG_TASK.fail(task_id,SQLCODE,SQLERRM);
  END disable_indexes2;
  
    /*
      Restore the indexes saved in disabled routine, remove the created file
     */  
   PROCEDURE restore_indexes(version_ varchar2) IS
      index_name CHAR(100) := '';
      indexed_table varchar(100);
      idx number;
      task_id number;
   BEGIN
     task_id := LOG_TASK.stared('EBD',version_,'Rebuild Indexes');
     FOR obj in (select * from object_ddls) LOOP
        DBMS_OUTPUT.PUT_LINE('Creating object '||obj.object_name);
        execute_ddl(obj.object_name);
     END LOOP; 
     
     --- loop for calculate stats
     idx := indexed_tables.FIRST;
      WHILE idx IS NOT NULL LOOP
         indexed_table := indexed_tables(idx);
         DBMS_STATS.GATHER_TABLE_STATS(USER, indexed_table);      
        idx := indexed_tables.NEXT(idx); 
      END LOOP;
      LOG_TASK.successed(task_id,sql%rowcount);
    EXCEPTION WHEN OTHERS THEN 
      LOG_TASK.fail(task_id,SQLCODE,SQLERRM);
  END restore_indexes;
  
  -- ===========================================================================
  
  PROCEDURE import_provisional(version_ varchar2) is
  task_id number;
  BEGIN
    task_id := LOG_TASK.stared('EBD',version_,'Import Provisional');
    -- @EBDprovisional.sql
    insert /*+ APPEND */ into ebird_basic_dataset 
      select /*+ INDEX (ebird_ebd_view obs_out_status_ndx)*/ * from ebird_ebd_view b
      where proj_id NOT IN ('EBIRD_GBBC','EBIRD_KIOSK') and valid=0 and 
         ((reviewed=0 and reason is null) or (reviewed=1 and reason is not null));
    LOG_TASK.successed(task_id,sql%rowcount);
    commit;
  EXCEPTION WHEN OTHERS THEN
    LOG_TASK.fail(task_id,SQLCODE,SQLERRM);
  END import_provisional;
  
  PROCEDURE import_all_years_gbbc(version_ varchar2) is
  task_id number;
  BEGIN
    task_id := LOG_TASK.stared('EBD',version_,'Import all years GBBC');
     -- @EBDpopAllYearsGBBC.sql;  
    insert /*+ APPEND */ into ebird_basic_dataset_gbbc 
      select /*+ INDEX (ebird_ebd_view obs_out_status_ndx)*/ * from ebird_ebd_view
        where proj_id = 'EBIRD_GBBC' and valid=1 
          and (reviewed=1 or (reviewed=0 and reason is null));
    LOG_TASK.successed(task_id,sql%rowcount);
    commit;
  EXCEPTION WHEN OTHERS THEN 
    LOG_TASK.fail(task_id,SQLCODE,SQLERRM);
  END import_all_years_gbbc;
  
  PROCEDURE import_provisional_gbbc(version_ varchar2) is
  task_id number;
  BEGIN
    task_id := LOG_TASK.stared('EBD',version_,'Import provisional GBBC');
    insert /*+ APPEND */ into ebird_basic_dataset_gbbc 
    select /*+ INDEX (ebird_ebd_view obs_out_status_ndx)*/ * from ebird_ebd_view 
      where valid=0 and proj_id = 'EBIRD_GBBC' and 
      ((reviewed=0 and reason is null) or (reviewed=1 and reason is not null));
    LOG_TASK.successed(task_id,sql%rowcount);
    commit;
  EXCEPTION WHEN OTHERS THEN 
    LOG_TASK.fail(task_id,SQLCODE,SQLERRM);
  END import_provisional_gbbc;
  
  PROCEDURE import_invalid(version_ varchar2) is
  task_id number;
  BEGIN
    task_id := LOG_TASK.stared('EBD',version_,'Import invalid');
    insert /*+ APPEND */ into ebird_basic_dataset_invalid 
        select  b.* from ebird_ebd_view b
          where b.proj_id NOT IN ('EBIRD_KIOSK') and 
          b.valid=0 and b.reviewed=1 and reason is null;
    commit;
    update ebird_basic_dataset_invalid i
    set reason = (select trr.name reason_description
                  from obs_review orr
                  join ref.tx_review_reason trr ON(trr.reason_code=orr.reason_code)
                  where i.obs_id=orr.obs_id );
    delete from ebird_basic_dataset_invalid where reason is null;
    commit;       
    LOG_TASK.successed(task_id,sql%rowcount);
    commit;
  EXCEPTION WHEN OTHERS THEN 
    LOG_TASK.fail(task_id,SQLCODE,SQLERRM);
  END import_invalid;
  
  
  PROCEDURE import_all_years(version_ varchar2, last_date date) is
  task_id number;
  range_start date := to_date('01/01/1800','MM/DD/YYYY');
  range_end date := to_date('01/01/2005','MM/DD/YYYY');
  BEGIN
    LOOP    
      task_id := LOG_TASK.stared('EBD',version_,'Import data from '||range_start
                                  ||' to '||range_end);
      
      insert /*+ APPEND */ into ebird_basic_dataset 
        select * from ebird_ebd_view
        where valid =1 and reviewed is not null and 
          proj_id NOT IN ('EBIRD_GBBC','EBIRD_KIOSK') 
           and obs_dt >= range_start and obs_dt < range_end
          ;
      LOG_TASK.successed(task_id,sql%rowcount);
      commit;
      
      range_start := range_end;
      range_end := range_end + interval '1' YEAR;
      
      if range_end > last_date then 
        range_end := last_date;
      end if;
      
      exit when range_start >= last_date;
      
    END LOOP;
       
  EXCEPTION WHEN OTHERS THEN 
    LOG_TASK.fail(task_id,SQLCODE,SQLERRM);
  END import_all_years;
  
  PROCEDURE import_all_years2(version_ varchar2, last_date date) is
  task_id number;
  range_start date := to_date('01/01/1800','MM/DD/YYYY');
  range_end date := to_date('01/01/2005','MM/DD/YYYY');
  BEGIN
      range_end := last_date;
      task_id := LOG_TASK.stared('EBD',version_,'Import data from '||range_start
                                  ||' to '||range_end);
      insert /*+ APPEND */ into ebird_basic_dataset 
        select /*+ INDEX (ebird_ebd_view obs_out_status_indx)*/ * from ebird_ebd_view
        where valid =1 and reviewed is not null and 
          proj_id NOT IN ('EBIRD_GBBC','EBIRD_KIOSK') 
          -- and obs_dt >= range_start and obs_dt < range_end
          ;
      LOG_TASK.successed(task_id,sql%rowcount);
      commit;       
  EXCEPTION WHEN OTHERS THEN 
    LOG_TASK.fail(task_id,SQLCODE,SQLERRM);
  END import_all_years2;
  
  PROCEDURE stats(version_ varchar2) is
  task_id number;
  BEGIN
    task_id := LOG_TASK.stared('EBD',version_,'Calculate stats');

    DELETE EBD_VERSION WHERE version = version_;
    INSERT INTO EBD_VERSION (VERSION, LATEST_VERSION_FLAG, LAST_REFRESH, 
       SUB_NUM, OBS_NUM, LOC_NUM, TAXA_NUM, SPECIES_NUM) 
    VALUES ( version_, '0', sysdate, 
      null, null, null, null, null );
   
    update EBD_VERSION set (obs_num, loc_num, taxa_num, sub_num) = 
        (select 
          count(distinct(obs_id)),
          count(distinct(loc_id)),
          count(distinct(ORIG_SPECIES_CODE)),
          count(distinct(sub_id))
        from ebird_basic_dataset )
      where version = version_;
    
    update EBD_VERSION set 
       species_num = (select count(distinct(s.species_code)) 
                      FROM EBIRD_BASIC_DATASET s where category = 'species') 
     where version = version_;
  
    -- Now update this version to be the latest version
    update EBD_VERSION set LATEST_VERSION_FLAG = '0';
    update EBD_VERSION set LATEST_VERSION_FLAG = '1'  
      where version = version_;
    LOG_TASK.successed(task_id,sql%rowcount);
    
    commit;
  
  -- check how it looks
  -- select * from ebd_version order by last_refresh; 
  EXCEPTION WHEN OTHERS THEN 
    LOG_TASK.fail(task_id,SQLCODE,SQLERRM);
  END stats;
  
   PROCEDURE import_records(version_ varchar2) is
   task_id number;
  BEGIN 
    task_id := LOG_TASK.stared('EBD',version_,'Import Records');
    execute immediate 'drop index ebird_ebd_status_indx ';
    execute immediate 'drop index ebird_ebd_obs_dt';
    execute immediate 'truncate table ebird_ebd';
    
    -- insert into ebird_ebd select * from ebird_ebd_view;
    LOG_TASK.successed(task_id,sql%rowcount);
    commit;
    
    execute immediate 'create index ebird_ebd_status_indx on ebird_ebd (valid,reviewed, proj_id)';
    execute immediate 'create index ebird_ebd_obs_dt on ebird_ebd (obs_dt)';    
  
  EXCEPTION WHEN OTHERS THEN 
    LOG_TASK.fail(task_id,SQLCODE,SQLERRM);
  END import_records;


  PROCEDURE start_gen(year_ number, month_ number) is
    version_ varchar2(50);
  BEGIN 
    version_ := INITCAP(TO_CHAR(TO_DATE(year_||'/'||month_, 'YYYY/MM'),'MON-YYYY'));
    
     -- refresh_views(version_);
    disable_indexes(version_);
    
    import_all_years(version_, LAST_DAY(TO_DATE(year_||'/'||month_, 'YYYY/MM'))+ interval '1' day);
    import_provisional(version_);
    import_invalid(version_); 
    import_provisional_gbbc(version_); 
    import_all_years_gbbc(version_);
    
    restore_indexes(version_);
    stats(version_); 
  END start_gen;
  /*
    get and store the ddl sentence of the given object
  */

END EBD_GENERATION;

/
