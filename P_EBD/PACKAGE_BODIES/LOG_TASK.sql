--------------------------------------------------------
--  DDL for Package Body LOG_TASK
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "LOG_TASK" AS

function stared(task_type varchar2, task_group varchar2, sub_task_name varchar2) 
return number is
tid number;
BEGIN
  tid := TASK_LOG_ID.NEXTVAL;
  INSERT INTO TASK_LOG(task_id, task_type, task_group, sub_task_name, status,start_date) 
  values (tid, task_type, task_group, sub_task_name,'running',current_timestamp);
  commit;
  return tid;
END stared;

PROCEDURE successed(taskId number, a_volumen number) 
is 
BEGIN
  UPDATE (select * from task_log where task_id=taskID) T
  set status='success', volumen=a_volumen, 
      duration = (current_timestamp-start_date)
      where task_id=taskId;
  commit;
END successed;

PROCEDURE fail(taskid number, code varchar2, a_message varchar2) 
is 
BEGIN
  UPDATE (select * from task_log where task_id=taskID) T
  set status='failed', message=code||': '||a_message,
    duration = (current_timestamp-start_date);
  commit;
END fail;

END LOG_TASK;

/
