--------------------------------------------------------
--  DDL for DB Link SW2.ORNITH.CORNELL.EDU
--------------------------------------------------------

  CREATE DATABASE LINK "SW2.ORNITH.CORNELL.EDU"
   CONNECT TO "P_EBIRD" IDENTIFIED BY VALUES ':1'
   USING 'SW2_SWIFT';
