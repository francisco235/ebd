--------------------------------------------------------
--  DDL for Table OBJECT_DDLS
--------------------------------------------------------

  CREATE TABLE "OBJECT_DDLS" 
   (	"OBJECT_NAME" VARCHAR2(255 BYTE), 
	"DDL_TEXT" VARCHAR2(4000 BYTE), 
	"GENERATED" TIMESTAMP (6), 
	"EXECUTED" TIMESTAMP (6), 
	"GROUP_NAME" VARCHAR2(255 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "CO2_DATA" ;
