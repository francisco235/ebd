--------------------------------------------------------
--  DDL for Table EBD_VERSION
--------------------------------------------------------

  CREATE TABLE "EBD_VERSION" 
   (	"VERSION" VARCHAR2(32 BYTE), 
	"LATEST_VERSION_FLAG" CHAR(1 BYTE), 
	"LAST_REFRESH" DATE, 
	"SUB_NUM" NUMBER, 
	"OBS_NUM" NUMBER, 
	"LOC_NUM" NUMBER, 
	"TAXA_NUM" NUMBER, 
	"SPECIES_NUM" NUMBER
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 0 INITRANS 2 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "EBIRD_BASIC_DATASET" ;

   COMMENT ON COLUMN "EBD_VERSION"."VERSION" IS 'Version identifier for the EBD';
   COMMENT ON COLUMN "EBD_VERSION"."LATEST_VERSION_FLAG" IS 'Flag defining which is the current version 1/0';
   COMMENT ON COLUMN "EBD_VERSION"."LAST_REFRESH" IS 'date/time of last refresh for this version';
   COMMENT ON COLUMN "EBD_VERSION"."SUB_NUM" IS 'Number of submissions in the EBD/Sampling Event data';
   COMMENT ON COLUMN "EBD_VERSION"."OBS_NUM" IS 'Number of observations in the EBD';
   COMMENT ON COLUMN "EBD_VERSION"."LOC_NUM" IS 'Number of distinct locations in the EBD';
   COMMENT ON COLUMN "EBD_VERSION"."TAXA_NUM" IS 'Number of distinct taxa in the EBD (includes issf, hybrids, spuhs, etc, count of orig_species_code)';
   COMMENT ON COLUMN "EBD_VERSION"."SPECIES_NUM" IS 'Number of distinct species in the EBD (count of species_code)';
   COMMENT ON TABLE "EBD_VERSION"  IS 'EBIRD BASIC DATASET version information,  the EBD is initially updated quarterly and details from each revision are stored in this table, including which version is the current version.';
