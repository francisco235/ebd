--------------------------------------------------------
--  DDL for Table OBS_REVIEW_NEW
--------------------------------------------------------

  CREATE TABLE "OBS_REVIEW_NEW" 
   (	"OBS_ID" VARCHAR2(32 BYTE), 
	"REASON_CODE" VARCHAR2(10 BYTE)
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS NOLOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "EBIRD_BASIC_DATASET" ;
