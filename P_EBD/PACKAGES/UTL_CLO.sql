--------------------------------------------------------
--  DDL for Package UTL_CLO
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "UTL_CLO" AS 

  TYPE StringArray IS VARRAY(255) OF VARCHAR2(255);

  -- Utilities for ddl management
  PROCEDURE save_ddl(the_object_name varchar2, the_object_type varchar2, a_group_name varchar2 default 'default_group') ;
  PROCEDURE execute_ddl(the_object_name varchar2, group_name varchar2 default 'default_group') ;
  PROCEDURE execute_group_ddl(a_group_name varchar2 default 'default_group') ;
  PROCEDURE delete_ddl_group(a_group_name varchar2 default 'default_group') ;

  -- Refresh of materialized view.
   PROCEDURE refresh_materialized_views(materialized_views StringArray);

  -- Utilities for index management
  PROCEDURE disable_indexes(indexed_table varchar2) ;
  PROCEDURE restore_indexes(indexed_table varchar2) ;

  PROCEDURE drop_index_if_exists(index_name varchar2) ; 
  PROCEDURE set_index_unusable(index_name varchar2) ;
  PROCEDURE rebuild_index(index_name varchar2) ;
  PROCEDURE set_table_indexes_unusable(a_table_name varchar2) ;
  PROCEDURE rebuild_table_indexes(a_table_name varchar2) ;

   -- utilities for table loading
  PROCEDURE truncate_table(table_name varchar2, reuse_storage boolean default true) ;
  PROCEDURE prepare_table_load(table_name varchar2) ;
  
END UTL_CLO;

/
