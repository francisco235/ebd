--------------------------------------------------------
--  DDL for Package EBD_GENERATION
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "EBD_GENERATION" AS 

  TYPE StringList IS VARRAY(25) OF VARCHAR2(100);
  -- Config 
  PROCEDURE start_gen(year_ number, month_ number);

  PROCEDURE refresh_views(version_ varchar2);  

  PROCEDURE disable_indexes(version_ varchar2);
  PROCEDURE restore_indexes(version_ varchar2);

  PROCEDURE import_provisional(version_ varchar2);
  PROCEDURE import_all_years_gbbc(version_ varchar2);
  PROCEDURE import_provisional_gbbc(version_ varchar2);
  PROCEDURE import_invalid(version_ varchar2);
  PROCEDURE import_all_years(version_ varchar2, last_date date);

  PROCEDURE stats(version_ varchar2);


  procedure save_ddl(the_object_name varchar2, the_object_type varchar2);
  procedure execute_ddl(the_object_name varchar2);

END EBD_GENERATION;

/
